db.fruits.insertMany([{
			name : "Apple",
			color : "Red",
			stock : 20,
			price: 40,
			supplier_id : 1,
			onSale : true,
			origin: [ "Philippines", "US" ]
		},

		{
			name : "Banana",
			color : "Yellow",
			stock : 15,
			price: 20,
			supplier_id : 2,
			onSale : true,
			origin: [ "Philippines", "Ecuador" ]
		},

		{
			name : "Kiwi",
			color : "Green",
			stock : 25,
			price: 50,
			supplier_id : 1,
			onSale : true,
			origin: [ "US", "China" ]
		},

		{
			name : "Mango",
			color : "Yellow",
			stock : 10,
			price: 120,
			supplier_id : 2,
			onSale : false,
			origin: [ "Philippines", "India" ]
		}]);

//The $count stage is equivalent to the following $group + $project sequence

db.fruits.aggregate(
	[
	{$match:{onSale: true}},
	{
	$count: "fruitsOnSale"
}
]);

//gte vs gt

db.fruits.aggregate(
[
{$match: {
	stock: {
		$gte: 20
	}
}},

{
	$count: "enoughStock"
},
]
	);

//avg

db.fruits.aggregate(
	[
	{$match:{onSale: true}},
	{$group: {
		_id: "$supplier_id", 
		avgAmount: { $avg: { $sum: [ "$price"] } }
	}
	}	
	]);


db.students.aggregate([
   { $project: { quizAvg: { $avg: "$quizzes"}, labAvg: { $avg: "$labs" }, examAvg: { $avg: [ "$final", "$midterm" ] } } }
])


//max operator

db.fruits.aggregate(
	[
	{$group: {
		_id: "$supplier_id",

		maxPrice: {$max: "$price"}
		}
	}
		
	]);

//min operator

db.fruits.aggregate(
	[
	{$group: {
		_id: "$supplier_id",

		minPrice: {$min: "$price"}
		}
	}
		
	]);


